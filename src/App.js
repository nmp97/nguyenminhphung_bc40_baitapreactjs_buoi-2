import './App.css';
import TryGlass from './Component/TryGlass';

function App() {
  return (
    <div className="App">
      <div className="overlay">
        <TryGlass />
      </div>
    </div>
  );
}

export default App;
