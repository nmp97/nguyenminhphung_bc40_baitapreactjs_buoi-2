import React, { Component } from 'react';
import glasses from '../Data/dataGlasses.json';

export default class TryGlass extends Component {
  state = { glasses, urlImgGlassModel: '' };

  renderGlasses = () => {
    let glassesList = this.state.glasses.map((glass) => {
      return (
        <div className="col-2" key={glass.id}>
          <img
            src={glass.url}
            alt=""
            onClick={() => {
              this.handleChangeGlass(glass.id);
            }}
          />
        </div>
      );
    });
    return glassesList;
  };

  handleChangeGlass = (id) => {
    let glass = this.state.glasses.filter((glass) => glass.id === id);

    this.setState({
      urlImgGlassModel: glass[0].url,
    });
  };

  render() {
    return (
      <div>
        <div className="title">
          <h1 className="fw-bold py-3">TRY GLASS APP ONLINE</h1>
        </div>
        <div className="container">
          <div className="imgModel d-flex justify-content-evenly py-4">
            <div className="model-1">
              <img src="./glassesImage/model.jpg" alt="" />
              <div className="glass-model">
                <img src={this.state.urlImgGlassModel} alt="" />
              </div>
            </div>
            <div className="model-2">
              <img src="./glassesImage/model.jpg" alt="" />
            </div>
          </div>
          <div className="glasses bg-light rounded p-3">
            <div className="row g-4">{this.renderGlasses()}</div>
          </div>
        </div>
      </div>
    );
  }
}
